import sys
import makeelf.elf as mkelf

def main(argv):
    elf = mkelf.ELF(
            e_class=mkelf.ELFCLASS.ELFCLASS32,
            e_data=mkelf.ELFDATA.ELFDATA2MSB,
            e_type=mkelf.ET.ET_REL,
            e_machine=mkelf.EM.EM_68K)
    strtab = elf.append_special_section('.strtab')
    symtab = elf.append_special_section('.symtab')
    rodata_data = bytes([
        0x6d, 0x79, 0x73, 0x74,  0x72, 0x69, 0x6e, 0x67, 0x00, 0x00, # mystring\0\0
        0,0,0,0])
    text = elf._append_section('.text', bytes([]), 0,
            sh_type=mkelf.SHT.SHT_PROGBITS,
            sh_flags=mkelf.SHF.SHF_ALLOC | mkelf.SHF.SHF_EXECINSTR,
            sh_addralign=4)
    text_sym = elf.append_symbol('.text', text, 0, 0,
            sym_binding=mkelf.STB.STB_LOCAL,
            sym_type=mkelf.STT.STT_SECTION)
    data = elf._append_section('.data', bytes([]), 0,
            sh_type=mkelf.SHT.SHT_PROGBITS,
            sh_flags=mkelf.SHF.SHF_ALLOC | mkelf.SHF.SHF_WRITE,
            sh_addralign=4)
    data_sym = elf.append_symbol('.data', data, 0, 0,
            sym_binding=mkelf.STB.STB_LOCAL,
            sym_type=mkelf.STT.STT_SECTION)
    bss = elf._append_section('.bss', bytes([]), 0,
            sh_type=mkelf.SHT.SHT_NOBITS,
            sh_flags=mkelf.SHF.SHF_ALLOC | mkelf.SHF.SHF_WRITE,
            sh_addralign=4)
    bss_sym = elf.append_symbol('.bss', bss, 0, 0,
            sym_binding=mkelf.STB.STB_LOCAL,
            sym_type=mkelf.STT.STT_SECTION)
    rodata = elf._append_section('.rodata', rodata_data, 0,
            sh_type=mkelf.SHT.SHT_PROGBITS,
            sh_flags=mkelf.SHF.SHF_ALLOC,
            sh_addralign=2)
    rodata_sym = elf.append_symbol('.rodata', rodata, 0, 0,
            sym_binding=mkelf.STB.STB_LOCAL,
            sym_type=mkelf.STT.STT_SECTION)
    sym_mydata = elf.append_symbol('mydata', rodata, 0x0a, 4,
            sym_binding=mkelf.STB.STB_GLOBAL,
            sym_type=mkelf.STT.STT_OBJECT)
    rela_data = bytes([0,0,0,0x0a, 0,0,rodata_sym,0x01, 0,0,0,0])
    rela = elf._append_section('.rela.rodata', rela_data, 0,
            sh_type=mkelf.SHT.SHT_RELA,
            sh_flags=mkelf.SHF.SHF_INFO_LINK,
            sh_info=rodata,
            sh_link=symtab,
            sh_addralign=4,
            sh_entsize=len(rela_data))
    sys.stdout.buffer.write(bytes(elf))

if __name__ == '__main__':
    main(sys.argv)
