OBJECTS=a.o b.o

main.bin: main Makefile
	m68k-none-elf-objcopy -O binary $< $@

main: $(OBJECTS) a.ld Makefile
	m68k-none-elf-ld $(LDFLAGS) --build-id=none -Ta.ld $(OBJECTS) -o $@

a.o: mkrelelf.py Makefile
	python $< >$@
	#echo 'const char * const mydata = "mystring";' | m68k-none-elf-gcc -c -o $@ -xc -

%.o: %.c Makefile
	m68k-none-elf-gcc $(CFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -rfv main main.bin $(OBJECTS)
